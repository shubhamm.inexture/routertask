import React from 'react';
import { NavLink, useNavigate } from 'react-router-dom';
const NavBar = (props) => {
    const navigate = useNavigate();

    const Logout = () => {
        props.setLogin(false);
        navigate('/signin');
    }
    const DashFunction = () => {
        props.setLogin(true);
        navigate('/p/dashboard');
    }
    return (
        <>
            <div className='text-center mt-3'>
                <NavLink className='btn-info m-3 btn-h-w' to='/'  >SignUp</NavLink>
                <NavLink className='btn-info m-3 btn-h-w' to='/signin'>SignIn</NavLink>
                {/* <NavLink className='btn-info m-3 btn-h-w' to='/p/dashboard'>DashBoard</
              NavLink> */}
                {
                    props.login ? <button className='btn-info m-3 btn-h-w' onClick={DashFunction}>DashBoard</button> : null
                }
                {
                    props.login ? <button className='btn-info m-3 btn-h-w' onClick={Logout}>logout</button> : null
                }
            </div>
        </>
    )
}
export default NavBar;