import "bootstrap/dist/css/bootstrap.min.css";
import "bootstrap/dist/js/bootstrap.bundle.min";
import './App.css';
import { Route, Routes } from 'react-router-dom';
import { useState } from 'react';
import NavBar from './NavBar';
import SignIn from './SignIn';
import SignUp from './SignUp';
import Private from './Private';
import DashBoard from './DashBoard';
import Error from './Error';

function App() {
  const [login, setLogin] = useState(false);
  return (
    <>
      <NavBar login={login} setLogin={setLogin} />
      <Routes>
        <Route path='/' element={<SignUp />} />
        <Route path='/signin' element={<SignIn setLogin={setLogin} />} />
        <Route path='/p' element={<Private login={login} />}>
          <Route path='dashboard' element={<DashBoard />} />
        </Route>
        <Route path='*' element={<Error />} />
      </Routes>
    </>
  )
}

export default App;
