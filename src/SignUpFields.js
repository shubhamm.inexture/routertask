import React from 'react';
import { ErrorMessage, useField } from 'formik';
const SignUpFields = ({ label, ...props }) => {
    const [field] = useField(props);
    // console.log(field,meta);
    return (
        <>
            <div className='row myBackground1'>
                <div className='container'>
                    <div className='col-md-4 offset-md-4'>
                        <div className='text-center '>
                            <div>
                                <label className='mt-3' htmlFor={field.name}>{label}</label>
                                <br />
                                <input className='input-field'
                                    {...field} {...props}
                                />
                            </div>
                            <div>
                                <ErrorMessage className='text-danger' name={field.name} />
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </>
    )
}


export default SignUpFields;