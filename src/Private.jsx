import React from 'react';
import { Outlet, Navigate } from 'react-router-dom';
const Private = (props) => {
    return (
        <>
            {
                props.login ? <Outlet /> : <Navigate to='/signin' />
            }
        </>
    )
}
export default Private;