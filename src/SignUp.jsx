import React from 'react';
import { Formik, Form } from 'formik';
import SignUpFields from './SignUpFields';
import * as Yup from 'yup';
import { useNavigate } from 'react-router-dom';

const SignUp = () => {

    const navigate = useNavigate();
    const validate = Yup.object({
        name: Yup.string()
            .required(<h5 className='text-danger'>'Please Fill the Field'</h5>),
        email: Yup.string()
            .email(<h5 className='text-danger'>'Invalid Email' </h5>)
            .required(<h5 className='text-danger'>'Please Fill the Field'</h5>),
        password: Yup.string()
            .required(<h5 className='text-danger'>'Please Fill the Field'</h5>),
        conformpassword: Yup.string()
            .oneOf([Yup.ref('password'), null],
                <h5 className='text-danger'>'Password Must Same'</h5>)
            .required(<h5 className='text-danger'>'Please Fill the Field'</h5>),
        number: Yup.string()
            .required(<h5 className='text-danger'>'Please Fill the Field'</h5>),
        state: Yup.string()
            .required(<h5 className='text-danger'>'Please Fill the Field'</h5>),
        city: Yup.string()
            .required(<h5 className='text-danger'>'Please Fill the Field'</h5>),
    })
    return (
        <>

            <Formik
                initialValues={{
                    name: '',
                    email: '',
                    password: '',
                    conformpassword: '',
                    number: '',
                    state: '',
                    city: ''
                }}
                validationSchema={validate}
                onSubmit={myValue => {
                    let userEncryption = JSON.stringify(myValue);
                    let EncryptionValue = btoa(userEncryption);
                    console.log(EncryptionValue);

                    localStorage.setItem("user", EncryptionValue);
                    navigate("/signin")
                }}
            >

                {
                    formik => (
                        <div>
                            {/* <h1>Sign Up Form</h1> */}
                            {console.log(formik.values)}
                            <Form>
                                <SignUpFields label="Enter Your Name:" name="name" type="text" />
                                <SignUpFields label="Enter Your Email:" name="email" type="email" />
                                <SignUpFields label="Enter Your Password:" name="password" type="password" />
                                <SignUpFields label="Enter Your ConformPassword:" name="conformpassword"
                                    type="password" />
                                <SignUpFields label="Enter Your Number:" name="number" type="number" />
                                <SignUpFields label="Enter Your State:" name="state" type="text" />
                                <SignUpFields label="Enter Your City:" name="city" type="text" />
                                <div className='text-center'>
                                    <button type='submit' className='btn-custom-design mt-3'>Submit</button>
                                </div>
                            </Form>
                        </div>

                    )
                }
            </Formik>
        </>
    )
}
export default SignUp;