import React, { useState } from 'react';
import { useNavigate } from 'react-router-dom';
const SignIn = (props) => {
    const navigate = useNavigate();
    const [name, setName] = useState();
    const [pass, setPass] = useState();
    const [error, setError] = useState('');
    const Validation = () => {
        let Dicription = localStorage.getItem("user")
        let DicriptionValue = JSON.parse(atob(Dicription))
        const userName = DicriptionValue.name;
        const password = DicriptionValue.password;

        if (name === userName && password === pass) {
            props.setLogin(true);
            navigate("/p/dashboard");
            setError("");
        }
        else {
            setError(<h1 className='text-danger'>"Not Valid Data"</h1>);
        }
    }

    return (
        <>
            <div>
                <div className='row '>
                    <div className='container'>
                        <div className='col-md-4 offset-md-4'>
                            <div className='text-center signin-up-div-design mt-5 myBackground' >
                                <h1>{error}</h1>
                                <label className='mt-3' htmlFor='name'>Enter Your Name:</label>
                                <input type="text" className='input-field' onChange={(e) => setName(e.target.value)} /><br />
                                <label className='mt-3' htmlFor='password'>Enter Your Password:</label>
                                <input type="password" className='input-field' onChange={(e) => setPass(e.target.value)} /><br />
                                <button type='submit' className=' mt-3 mb-2 btn-custom-design' onClick={Validation}>Submit</button>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </>
    )
}
export default SignIn;